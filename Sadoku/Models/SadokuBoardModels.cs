﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.Text;

namespace Sadoku.Models
{

    public class SadokuBoardViewModel : SadokuBoard
    {
        public int[][] BoardArr { get; set; }
        

        public SadokuBoardViewModel()
        {
        }

        public SadokuBoardViewModel(SadokuBoard sadokuBoard)
        {
            IniBoard = sadokuBoard.IniBoard;
            BoardArr = _createBoardArr(sadokuBoard.IniBoard);
        }

        public void Resolved()
        {
            BoardArr = _createBoardArr(FinBoard);
        }

        static private int[][] _createBoardArr(String board)
        {
            int[][] boardArr = new int[9][];

            for (int i = 0; i < 9; i++)
            {
                boardArr[i] = new int[9];
                for (int j = 0; j < 9; j++)
                {
                    char c = board[i * 9 + j];
                    int n = c - '0';
                    boardArr[i][j] = n;
                }
            }
            return boardArr;
        }

    }




    /*
    [DataContract]
    class SadokuBoardMetadataType
    {

    }

    [MetadataType(typeof(SadokuBoardMetadataType))]
    public partial class SadokuBoard
    {
    }
     */
}
    
